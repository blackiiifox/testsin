﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using SinTest.Model.DataModel;

namespace SinTest.ViewModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Threading;

    using OxyPlot;
    using OxyPlot.Axes;
    using OxyPlot.Series;

    class MainViewModel : INotifyPropertyChanged, IDisposable
    {

        #region Variables
        public string TextMy { get; set; } = "Генератор";
        private const int UpdateInterval = 33;
        private int maxNumberOfPoints = 1000;
        #endregion

        #region Object Variables
        private DataModel SignalGen = new DataModel();
        private bool disposed;
        private readonly Timer timer;
        private readonly Stopwatch watch = new Stopwatch();
        private int numberOfSeries;
        #endregion

       
        #region Setters
        public PlotModel PlotModel { get; private set; }
        public int TotalNumberOfPoints { get; private set; }
        public int MaxTimeOnGraph
        { get
            {
               return maxNumberOfPoints;
            }
            set
            {
                maxNumberOfPoints = value;
            }
        }

        public string CurrentFilter
        {
            get { return SignalGen.CurrentFilter; }
            set { SignalGen.CurrentFilter = value; }
        }

        public ObservableCollection<string> Filters
        {
            get { return SignalGen.ListNamesFilters; }
        }

        public bool CheckedRealTime
        {
            get { return SignalGen.RealTime; }
            set { SignalGen.RealTime = value; }
        }

        public bool CheckedNoise
        {
            get { return SignalGen.StateNoise; }
            set { SignalGen.StateNoise = value; }
        }

        public bool CheckedFilter
        {
            get { return SignalGen.StateFilter; }
            set { SignalGen.StateFilter = value; }
        }

        public string WindowsSize
        {
            get {
                return SignalGen.WindowSize.ToString();
            }
            set {
                SignalGen.WindowSize = Convert.ToInt32(value);
            }
        }

        public string OrderCount
        {
            get
            {
                return SignalGen.Order.ToString();
            }
            set
            {
                SignalGen.Order = Convert.ToInt32(value);
            }
        }

        public string SignalAmp
        {
            get
            {
                return SignalGen.SignalAmpl.ToString();
            }
            set
            {
                SignalGen.SignalAmpl = Convert.ToDouble(value);
            }
        }

        public string NoiseAmp
        {
            get
            {
                return SignalGen.NoiseAmpl.ToString();
            }
            set
            {
                SignalGen.NoiseAmpl = Convert.ToDouble(value);
            }
        }

        public string SampleRate
        {
            get
            {
                return SignalGen.SampleRate.ToString();
            }
            set
            {
                SignalGen.SampleRate = Convert.ToDouble(value);
            }
        }

        public string SignalFreq
        {
            get
            {
                return SignalGen.SignalFreq.ToString();
            }
            set
            {
                SignalGen.SignalFreq = Convert.ToDouble(value);
            }
        }

        public string NoiseFreq
        {
            get
            {
                return SignalGen.NoiseFreq.ToString();
            }
            set
            {
                SignalGen.NoiseFreq = Convert.ToDouble(value);
            }
        }

        #endregion

        public MainViewModel()
        {
            // SignalGen = new DataModel(1000);
            this.timer = new Timer(OnTimerElapsed);
            SetupModel();
            //SignalGen.StartGenericSin();
            //Update();
            //Update();
        }


        #region Events
        private void PushButtonStart()
        {

            this.timer.Change(1000, UpdateInterval);
        }

        private void PushButtonStop()
        {
            this.timer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        private void PushButtonGenSin()
        {
            SignalGen.StartGenericSin();
            
        }

        private void EnableFilterTest()
        {
            SignalGen.TestFilter();
        }

        private void RedwarGraphic()
        {
            SignalGen.CalculateSin();
            OnTimerElapsed(false);
            //this.Update();
            // SignalGen.StartGenericSin();
        }
        #endregion

        #region Commands
        public ICommand StopCommand
        {
            get { return new CommandHandler(() => PushButtonStop(), true); }
        }

        public ICommand StartCommand
        {
            get { return new CommandHandler(() => PushButtonStart(), true); }
        }

        public ICommand GenSinCommand
        {
            get { return new CommandHandler(() => PushButtonGenSin(), true); }
        }

        public ICommand FilterTestCommand
        {
            get { return new CommandHandler(() => EnableFilterTest(), true); }
        }

        public ICommand RedrawCommand
        {
            get { return new CommandHandler(() => RedwarGraphic(), true); }
        }
        #endregion

        #region GraphicInit
        private void SetupModel()
        {
            this.timer.Change(Timeout.Infinite, Timeout.Infinite);

            PlotModel = new PlotModel();
            PlotModel.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Minimum = -3000, Maximum = 3000 });

            this.numberOfSeries = 1;

            for (int i = 0; i < this.numberOfSeries; i++)
            {
                PlotModel.Series.Add(new LineSeries { LineStyle = LineStyle.Solid });
            }

            this.watch.Start();

            this.RaisePropertyChanged("PlotModel");

            
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string property)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(property));
            }
        }
        #endregion

        #region Draw
        private void OnTimerElapsed(object state)
        {
            lock (this.PlotModel.SyncRoot)
            {
                this.Update();
            }

            this.PlotModel.InvalidatePlot(true);
        }

        private void Update()
        {
            
            double t = this.watch.ElapsedMilliseconds * 0.001;
            int n = 0;

            for (int i = 0; i < PlotModel.Series.Count; i++)
            {
                var s = (LineSeries)PlotModel.Series[i];
                List<double> data;
                if (SignalGen.RealTime == false)
                {
                    s.Points.Clear();
                    data = SignalGen.GetData();
                }
                else
                {
                    data = SignalGen.GetNewData();
                }

                for (int j = 0; j < data.Count(); j++)
                {
                    double x = s.Points.Count > 0 ? (s.Points[s.Points.Count - 1].X + 1) : 0;

                    if (s.Points.Count >= maxNumberOfPoints)
                        s.Points.RemoveAt(0);

                    double y = data[j];
                    s.Points.Add(new DataPoint(x, y));
                }
                n += s.Points.Count;

            }
            /*
            for (int i = 0; i < PlotModel.Series.Count; i++)
            {
                var s = (LineSeries)PlotModel.Series[i];


                double x = s.Points.Count > 0 ? s.Points[s.Points.Count - 1].X + 1 : 0;
                if (s.Points.Count >= 200)
                    s.Points.RemoveAt(0);
                double y = 0;
                int m = 80;
                for (int j = 0; j < m; j++)
                    y += Math.Cos(0.001 * x * j * j);
                y /= m;
                s.Points.Add(new DataPoint(x, y));

                n += s.Points.Count;
            }
            */
            if (this.TotalNumberOfPoints != n)
            {
                this.TotalNumberOfPoints = n;
                this.RaisePropertyChanged("TotalNumberOfPoints");
            }

        }
        #endregion

        #region Dispose
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.timer.Dispose();
                }
            }

            this.disposed = true;
        }
        #endregion

        public class CommandHandler : ICommand
        {
            private Action _action;
            private bool _canExecute;
            public CommandHandler(Action action, bool canExecute)
            {
                _action = action;
                _canExecute = canExecute;
            }

            public bool CanExecute(object parameter)
            {
                return _canExecute;
            }

            public event EventHandler CanExecuteChanged;

            public void Execute(object parameter)
            {
                _action();
            }
        }
    }
}
