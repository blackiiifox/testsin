﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinTest.Model
{

	class LoaderModel
	{
		private List<FiltersParametr> data = new List<FiltersParametr>();

        public void Add(double sR, double fW, double ord, double dist)
        {
            FiltersParametr new_elem = new FiltersParametr(sR, fW, ord, dist);
            data.Add(new_elem);
        }

		public void Save(string filePath)
		{
			var csv = new StringBuilder();
			var newLine = string.Format("sampleRate;filterWindow;order;distances");
			csv.AppendLine(newLine);
			for (int i = 0; i < data.Count; i++)
			{
				newLine = string.Format("{0};{1};{2};{3}", data[i].sampleRate, data[i].filterWindow, data[i].order, data[i].distances);
				csv.AppendLine(newLine);
			}
			File.WriteAllText(filePath, csv.ToString());
		}

		public LoaderModel(){}
	}

	class FiltersParametr
	{
		public double sampleRate;
		public double filterWindow;
		public double order;
		public double distances;

		public FiltersParametr(double sR, double fW, double ord, double dist)
		{
			sampleRate = sR;
			filterWindow = fW;
			order = ord;
			distances = dist;
		}
	}
}
