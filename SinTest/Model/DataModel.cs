﻿using MathNet.Filtering;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;


namespace SinTest.Model.DataModel
{

    class DataModel
    {
        #region Variables
        private const int testFiltercMod = 0;
        private List<double> data = new List<double>();
        private double sampleRate = 2000;
        private bool stateNoise = false;
        private bool stateRealTime = false;
        private bool stateFilter = false;

        // Signal variable
        private double noiseFreq = 50;
        private double noiseAmpl = 2000;
        private double signalFreq = 100;
        private double signalAmpl = 1000;
        private readonly int countsNumber = 1000;
        
        LoaderModel saveModel = new LoaderModel();

        private int indexDataReturned = 0;
        DispatcherTimer timer = new DispatcherTimer();
        private ObservableCollection<string> comPorts = new ObservableCollection<string>();

        // Filters variable
        private const string MathNetLowPassFilter = "MathNetLow";
        private const string MathNetHighPassFilter = "MathNetHigh";
        private const string Envelope50HzFilter = "Envelope50HzFilter";
        private const string MathNetBandPassFilter = "MathNetBandPass";
        private const string MathNetBandStopFilter = "MathNetBandStop";
        private ObservableCollection<string> listFilters = new ObservableCollection<string> { MathNetLowPassFilter, MathNetHighPassFilter, MathNetBandPassFilter, MathNetBandStopFilter, Envelope50HzFilter };

        private string currentFilter = MathNetHighPassFilter; // 0 - low filter, 1 - band pass,2 - high
        private int windowSize = 150;
        private int order = 20;
        private OnlineFilter Filter { get; set; }


        #endregion

        #region Setters
        public double SampleRate
        {
            get
            { return sampleRate; }
            set
            {   sampleRate = value; }
        }

        public string NumFilter
        {
            get { return currentFilter; }
            set { currentFilter = value;  }
        }

        public bool RealTime
        {
            get { return stateRealTime; }
            set { stateRealTime = value; }
        }

        public ObservableCollection<string> ComPorts
        {
            get { return comPorts; }
            private set { comPorts = value; }
        }

        public string CurrentFilter
        {
            get { return currentFilter; }
            set { currentFilter = value; }
        }

        public ObservableCollection<string> ListNamesFilters
        {
            get { return listFilters; }
            private set { listFilters = value; }
        }

        public bool StateNoise
        {
            get { return stateNoise; }
            set { stateNoise = value; }
        }

        public bool StateFilter
        {
            get { return stateFilter; }
            set { stateFilter = value; }
        }

        public int WindowSize
        {
            get { return windowSize; }
            set { windowSize = value; }
        }

        public int Order
        {
            get { return order; }
            set { order = value; }
        }

        public double NoiseFreq
        {
            get { return noiseFreq; }
            set { noiseFreq = value; }
        }

        public double NoiseAmpl
        {
            get { return noiseAmpl; }
            set { noiseAmpl = value; }
        }

        public double SignalFreq
        {
            get { return signalFreq; }
            set { signalFreq = value; }
        }

        public double SignalAmpl
        {
            get { return signalAmpl; }
            set { signalAmpl = value; }
        }

        #endregion

        #region Constructor
        public DataModel()
        {

        }

        public DataModel(double newSampleRate)
        {
            this.sampleRate = newSampleRate;
            // saveModel = new LoaderModel();
		}
        #endregion

        public void StartGenericSin()
        {
            if (testFiltercMod == 1)
            {
                TestFilter();
            }
            else
            {
                TimerCallback timeCB = new TimerCallback(TimerTick);
                Timer time = new Timer(timeCB, null, 0, 1);
                //timer.Tick += new EventHandler(TimerTick);
                // timer.Interval = TimeSpan.FromSeconds(0.001);
                // timer.Interval = TimeSpan.FromMilliseconds(1);
                //timer.Interval = TimeSpan.FromMilliseconds((1.0/sampleRate)*1000);
                //timer.Start();
                // for (int i = 0; i < 100; i++)
                //     data.Add(1);
            }
        }

        int i1 = 0;
        private void TimerTick(object sender)
        {
            if (data.Count % 100 == 0)
                i1++;
            lock (data)
            {
                data.Add(i1);
            }
        }

        public void CalculateSin()
        {
            List<double> idealSin = new List<double>();
            List<double> errorlSin = new List<double>();
            data.Clear();
            // Генерируем первичные сигналы.
            for (int i = 0; i < countsNumber; i++)
            {
                double omega = Math.PI * 2 * i;
                double signal = signalAmpl * Math.Sin((omega * signalFreq) / countsNumber);  // + (140 * Math.Sin((omega * 200) / countsNumber)) + (800 * Math.Sin((omega * 120) / countsNumber));
                
                double val = signal;
                if (stateNoise == true)
                {
                    double noise = noiseAmpl * Math.Sin((omega * noiseFreq) / countsNumber);
                    val = val + noise;
                }
                data.Add(val);
            }

            if (stateFilter == true)
            {
                switch(currentFilter)
                {
                    case MathNetLowPassFilter:
                        Filter = OnlineFilter.CreateLowpass(ImpulseResponse.Finite, sampleRate, this.WindowSize, this.order);
                        break;

                    case MathNetHighPassFilter:
                        Filter = OnlineFilter.CreateHighpass(ImpulseResponse.Finite, sampleRate, this.WindowSize, this.order);
                        break;

                    case MathNetBandPassFilter:
                        Filter = OnlineFilter.CreateBandpass(ImpulseResponse.Finite, sampleRate, noiseFreq - this.WindowSize, noiseFreq + this.WindowSize, this.order);
                        break;


                    case Envelope50HzFilter:
                        Filter = OnlineFilter.CreateLowpass(ImpulseResponse.Finite, sampleRate, 100);
                        break;
                        
                    case MathNetBandStopFilter:
                        Filter = OnlineFilter.CreateBandstop(ImpulseResponse.Finite, sampleRate, sampleRate - this.WindowSize, sampleRate + this.WindowSize, this.order);
                        break;
                }
                
                Filter.Reset();
                for (int i = 0; i < countsNumber; i++)
                {
                    if (currentFilter == Envelope50HzFilter)
                    {
                        data[i] = Filter.ProcessSample(data[i]);
                        // double envelope = Math.Abs(data[i] - 20000);
                        // data[i] = Filter.ProcessSample(envelope);
                    }
                    else
                    {
                        data[i] = Filter.ProcessSample(data[i]);
                    }
                    


                }
            }
            /*
             * for (int i = 0; i < countsNumber; i++)
            {
            }
                data = idealSin;*/
        }

        public void TestFilter()
        {
            List<double> idealSin = new List<double>();
            List<double> errorlSin = new List<double>();

            // Генерируем первичные сигналы.
            for (int i = 0; i < countsNumber; i++)
            {
                double omega = Math.PI * 2 * i;
                double signal = signalAmpl * Math.Sin((omega * signalFreq) / countsNumber);
                double noize = noiseAmpl * Math.Sin((omega * noiseFreq) / countsNumber);
                double val = signal + noize;
                idealSin.Add(signal);
                errorlSin.Add(val);
            }
            
            double filterWindow = 20;
            int order = 3;
            // Как будто в цикле 
            for (filterWindow = 0; filterWindow < 50; filterWindow++)
                for (order = 0; order < 101; order++)
                {
                    Filter = OnlineFilter.CreateBandpass(ImpulseResponse.Finite, sampleRate, noiseFreq - filterWindow, noiseFreq + filterWindow, order);
                    Filter.Reset();
                    List<double> testSignal = new List<double>();
                    double testVal = 0;
                    for (int i = 0; i < countsNumber; i++)
                    {
                        testVal = Filter.ProcessSample(errorlSin[i]);
                        testSignal.Add(testVal);
                    }
                    double distance = EuclideanDistance(idealSin, testSignal);
                    data = idealSin;

                    saveModel.Add(sampleRate, filterWindow, order, distance);
                    //saveModel.Add(5, 6, 7, 8);
                    //saveModel.Add(9, 10, 11, 12);
                }
            saveModel.Save(Environment.CurrentDirectory + "\\test.csv");
        }

        private double EuclideanDistance(List<double> signal1, List<double> signal2)
        {
            double distance = 0;
            int max_count = signal1.Count();
            if (signal1.Count() != signal2.Count())
                return -1;

            
            for (int i = 0; i < max_count; i++)
            {
                distance += Math.Pow(signal1[i] - signal2[i], 2);
            }
            distance = Math.Sqrt(distance);

            return distance;
        }

        private void GenericSin()
        {
            
        }

        public List<double> GetNewData()
        {
            lock (data)
            {
                int currIndexReturnder = indexDataReturned;
                indexDataReturned = data.Count;
                var new_data = data.Skip(currIndexReturnder).Take(data.Count).ToList<double>();
                return new_data;
            }
            
        }

        public List<double> GetData()
        {
            return data;
        }
    }

    
}
