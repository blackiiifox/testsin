﻿using SinTest.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SinTest.View
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = new MainViewModel();
        }

        private void RealTimeCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            ButStart.Visibility = Visibility.Visible;
            ButStop.Visibility = Visibility.Visible;
            ButSinSimulation.Visibility = Visibility.Visible;
            COMPortList.Visibility = Visibility.Visible;

            RedrawBut.Visibility = Visibility.Hidden;
            FilterTesetBut.Visibility = Visibility.Hidden;
            CheckboxNoise.Visibility = Visibility.Hidden;
            CheckboxFilters.Visibility = Visibility.Hidden;
            TextBoxOrder.Visibility = Visibility.Hidden;
            TextBoxWindow.Visibility = Visibility.Hidden;
            LabelOrder.Visibility = Visibility.Hidden;
            LabelNoise.Visibility = Visibility.Hidden;
            // GroupBoxSetting.Visibility = Visibility.Hidden;


        }

        private void RealTimeCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            ButStart.Visibility = Visibility.Hidden;
            ButStop.Visibility = Visibility.Hidden;
            ButSinSimulation.Visibility = Visibility.Hidden;
            COMPortList.Visibility = Visibility.Hidden;

            RedrawBut.Visibility = Visibility.Visible;
            FilterTesetBut.Visibility = Visibility.Visible;
            CheckboxNoise.Visibility = Visibility.Visible;
            CheckboxFilters.Visibility = Visibility.Visible;
            TextBoxOrder.Visibility = Visibility.Visible;
            TextBoxWindow.Visibility = Visibility.Visible;
            LabelNoise.Visibility = Visibility.Visible;
            LabelOrder.Visibility = Visibility.Visible;
            // GroupBoxSetting.Visibility = Visibility.Visible;

        }
    }
}
